# Dockerize Ruby on Rails application

Simple step by step setup Ruby on Rails application by [Docker](https://www.docker.com)

1. Intro
2. Dockerfile
3. Docker compose
4. Cache bundle and assets
5. First Setup
6. Using

## Intro

Stack:
- Ruby | 2.5
- PostgreSQL | 9.6
- Redis | 3.2
- Sidekiq
- ImageMagick

## Dockerfile

Initialize `Dockerfile`

```bash
touch Dockerfile
```

Use an official [ruby:2.5-slim](https://hub.docker.com/_/ruby) image as a parent image

`Dockerfile`
```dockerfile
FROM ruby:2.5-slim
```

Define list of packages that need for correct work of Ruby on Rails application to environment variable `BUILD_PACKAGES`

`Dockerfile`
```dockerfile
ENV BUILD_PACKAGES build-essential \ 
                   libpq-dev \
                   libxml2-dev \
                   libxslt1-dev \
                   nodejs \
                   imagemagick
```

Set command for install packages

`Dockerfile`
```dockerfile
RUN apt-get update -qq && apt-get install -y $BUILD_PACKAGES
```

Set the working directory to `/app_name`

`Dockerfile`
```dockerfile
ENV APP_HOME /app_name
WORKDIR $APP_HOME
ADD . $APP_HOME
```

Add bundle entry point to handle `bundle install`

`Dockerfile`
```dockerfile
ENV BUNDLE_PATH=/vendor/bundle \
    GEM_HOME=/bundle

COPY ./docker-bundle-entrypoint.sh /
RUN chmod +x /docker-bundle-entrypoint.sh
ENTRYPOINT ["/docker-bundle-entrypoint.sh"]
```

In `docker-bundle-entrypoint.sh`

```sh
#!/bin/bash
# Interpreter identifier

# Exit on fail
set -e

# Ensure all gems installed. Add binstubs to bin which has been added to PATH in Dockerfile.
bundle check || bundle install --path "$BUNDLE_PATH"

# Finally call command issued to the docker service
exec "$@"
```

## Docker-compose
Initialize `docker-compose.yml` in which will be up all related services.

```bash
touch docker-compose.yml
```

Set Docker compose [version](https://docs.docker.com/compose/compose-file/compose-versioning)

```yml
version: '3.1'
```

Define services which will run with start of current application:
- PostgreSQL database
- Redis

### PostgreSQL service

`docker-compose.yml`
```yml
services:
  db:
    image: postgres:9.6
    ports:
      - 5428:5432
    environment:
      POSTGRES_USER: postgres
      POSTGRES_PASSWORD: postgres
      POSTGRES_DB: app_name_development
```

In `ports` we announce which port on HostOS will be proxy to GuestOS. For example, PostgreSQL in container `db` is availabled by `5432` port, in machine PostgreSQL from container `db` will be available through port `5428`.

### Redis service

The same for redis

`docker-compose.yml`
```yml
  redis:
    image: redis:3.2-alpine
    ports:
      - "6378:6379"
```

Define configs for api service

`docker-compose.yml`
```yml
  api:
    build: .
    links:
      - db
      - redis
    volumes:
      - .:/app_name
    tty: true
    stdin_open: true
    command: bundle exec sidekiq -C config/sidekiq.yml
    environment:
      PG_HOST: db
      PG_PORT: "5432"
      PG_USERNAME: postgres
      PG_PASSWORD: postgres
      REDIS_DB: "redis://redis:6379"
```

### Sidekiq
In app we will use [Sidekiq](https://github.com/mperham/sidekiq). So we need to add additional services `sidekiq`. Service `sidekiq` will be the same as service `api`. You can move the similar options to `api_app` for reuse in `api_sidekiq` and `api` services.

`docker-compose.yml`
```yml
  api_app: &api_app
    build: .
    links:
      - db
      - redis
    volumes:
      - .:/app_name
    tty: true
    stdin_open: true
    environment:
      PG_HOST: db
      PG_PORT: "5432"
      PG_USERNAME: postgres
      PG_PASSWORD: postgres
      REDIS_DB: "redis://redis:6379"

  api:
    <<: *api_app
    command: bundle exec rails server --port 3000 --binding 0.0.0.0
    ports:
      - 3000:3000

  api_sidekiq:
    <<: *api_app
    command: bundle exec sidekiq -C config/sidekiq.yml
```

## Cache bundle and assets

While dockering a Rails app, the first problem that comes out is the slow bundle install command while building the app’s image. 

For make building more faster we can move installed gems to separate [volume](https://docs.docker.com/storage/volumes) `api_bundle_cache`.

Create global volume in which we place installed gems.

```yml
version: '3.1'

volumes:
  api_bundle_cache:
```

Define directory(`/vendor/bundle`) that will be moved to volume `api_bundle_cache`

```yml
api_app: &api_app
  build: .
  links:
    - db
  volumes:
    - .:/app_name
    - api_bundle_cache:/vendor/bundle
  # ...
```

## First Setup

Build images and containers which are defined in composition file `docker-compose.yml`

```bash
docker-compose build
```

Run all services `db`, `redis`, `api`, `api_sidekiq`.

```bash
docker-compose up
```

Execute `rails db:migrate` in container `api`

```bash
docker-compose exec web bundle exec rails db:migrate
```

Execute `rails db:seed` in container `api`

```bash
docker-compose exec web bundle exec rails db:seed
```

## Using

If need stop all services

```bash
docker-compose stop
```

If need stop remove all containers

```bash
docker-compose down
```

If need run all services

```bash
docker-compose up
```
