# Initialize ruby image
FROM ruby:2.5-slim

# Install Rails dependencies packages
ENV BUILD_PACKAGES build-essential libpq-dev libxml2-dev libxslt1-dev nodejs imagemagick

RUN apt-get update -qq && apt-get install -y $BUILD_PACKAGES

# Define and add workdir
ENV APP_HOME /spree-demo
WORKDIR $APP_HOME
ADD . $APP_HOME

# Add bundle entry point to handle bundle cache
ENV BUNDLE_PATH=/vendor/bundle \
    GEM_HOME=/bundle

COPY ./docker-bundle-entrypoint.sh /
RUN chmod +x /docker-bundle-entrypoint.sh
ENTRYPOINT ["/docker-bundle-entrypoint.sh"]
